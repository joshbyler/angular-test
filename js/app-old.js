(function() {

    angular.module('imagePicker', ['restangular', 'angular-loading-bar'])
        .config(['RestangularProvider', function(RestangularProvider) {
            RestangularProvider.setBaseUrl('http://cmsapi.bmwusa.williams-forrest.com/api/cms/');
        }])
        .controller('foldersController', function($scope, Restangular) {
            if (!$scope.documentLibrary) {
                $scope.documentLibrary = {};
                $scope.breadcrumb = {};

                //grab all the folders
                var allFolders = Restangular.one('folder');
                var assetFolderID;
                allFolders.get({foldersOnly: true}).then(function(data) {

                    //grab the assets folder id
                    for (var i = 0; i < data['childFolders'].length; i++) {
                        if (data['childFolders'][i].name === 'assets') {
                            assetFolderID = data['childFolders'][i].id;
                            $scope.assetFolderID = assetFolderID;
                            getAssetFolder();
                            break;
                        }
                    }
                });

                //grab the asset folder
                function getAssetFolder() {
                    var imagesFolderID;
                    var assetFolder = Restangular.one('folder/' + $scope.assetFolderID);
                    //grab the images folder
                    assetFolder.get().then(function(data) {
                        for (var i = 0; i < data['childFolders'].length; i++) {
                            if (data['childFolders'][i].name === 'images') {
                                imagesFolderID = data['childFolders'][i].id;
                                $scope.imagesFolderID = imagesFolderID;
                                getImagesFolder();
                                break;
                            }
                        }
                    });
                }

                function getImagesFolder() {
                    var imagesFolder = Restangular.one('folder/' + $scope.imagesFolderID);
                    imagesFolder.get().then(function(data) {
                        $scope.activeParent = data['childFolders'][0].id;
                        $scope.documentLibrary.childFolders = data['childFolders'];
                    });
                }


            }
        })
        .directive('folderViewer', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/folderViewer.html',
                controller: 'foldersController',
                link: function($scope, $elem, $attrs) {}
            };
        }])
        // .controller('imagesController', function($scope, Restangular) {
        //     //console.log('images controller');
        //     $scope.getFolderImages = function(id) {
        //
        //         if (id !== $scope.activeParent) {
        //             $scope.activeParent = id;
        //
        //             var currentImages = Restangular.one('folder/' + id);
        //             currentImages.get().then(function(data) {
        //                 $scope.documentLibrary.imagesChildFolders = data['childFolders'];
        //                 // console.log(data);
        //             });
        //         }
        //     }
        // })
        // .directive('imagesViewer', ['$document', function($document) {
        //     //console.log('images directive');
        //     return {
        //         replace: true,
        //         restrict: 'E',
        //         templateUrl: 'templates/imagesViewer.html',
        //         controller: 'imagesController',
        //         link: function($scope, $elem, $attrs) {}
        //     };
        // }])
        .controller('innerImagesController', function($scope, Restangular) {
            //console.log('images inner controller');
            // $scope.documentLibrary.innerImages = {};
            // $scope.getImages = function(id) {
            //     var innerImages = Restangular.one('folder/' + id);
            //     innerImages.get().then(function(data) {
            //         //console.log(data);
            //         $scope.documentLibrary.innerImages = data['documents'];
            //     });
            // }
        })
        .directive('innerImagesViewer', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/innerImagesViewer.html',
                controller: 'innerImagesController',
                link: function($scope, $elem, $attrs) {
                }
            };
        }])
        .controller('imagesFoldersViewerPanelHeadingController', function($scope, Restangular) {
            $scope.updateCurrentParent = function(id, event) {
                event.preventDefault();

                if (id && id !== $scope.activeParent) {
                    $scope.activeParent = id;
                    $scope.$broadcast('parent-updated', {activeParent: id});

                    //toggle all the folders so that only the currently selected folder is open
                    $('.list-group-item a').removeClass('collapsed').addClass('collapsed');
                    event.currentTarget.classList.toggle('collapsed');
                }

                //breadcrumbs
                //TODO - have to get the breadcrumb built. also be able to tear it down.
                console.log($scope.breadcrumb);
                var breadcrumbQuery = Restangular.one('folder/' + $scope.activeParent);
                breadcrumbQuery.get().then(function(data) {
                    console.log(data);
                    $scope.breadcrumb += data['name'];
                });
                console.log($scope.breadcrumb);
            }
            $scope.$watch('activeParent', function(newValue, oldValue){
                if(newValue !== oldValue) {
                    Restangular.one('folder/', newValue).get().then(function(data){
                        $scope.folderView = data.plain();
                        $scope.folderContext = data['contextPath'];

                        //If we don't have any child folders, then we're at the images level, and need to put that up in the $scope
                        if (data['childFolders'].length === 0) {
                            $scope.documentLibrary.innerImages = data['documents'];
                        }
                    });
                }
            });
        })
        .directive('imagesFoldersViewerPanelHeading', function() {
            return {
                replace: false,
                restrict: 'A',
                templateUrl: 'templates/imagesFoldersViewerPanelHeading.html',
                controller: 'imagesFoldersViewerPanelHeadingController',
                link: function($scope, $elem, $attrs) {}
            };
        })
        .controller('imagesFoldersViewerPanelContentController', function($scope, Restangular) {

        })
        .directive('imagesFoldersViewerPanelContent', function() {
            return {
                replace: false,
                restrict: 'A',
                templateUrl: 'templates/imagesFoldersViewerPanelContent.html',
                controller: 'imagesFoldersViewerPanelContentController',
                link: function($scope, $elem, $attrs) {
                    $scope.$on('parent-updated', function(e, args){
                        if ($scope.folder.id === args.activeParent) {
                            //console.log("yea");
                        }
                    });
                }
            };
        })
        .controller('imagesFoldersViewerPanelContentItemController', function($scope, Restangular) {

        })
        .directive('imagesFoldersViewerPanelContentItem', function() {
            return {
                replace: false,
                restrict: 'A',
                templateUrl: 'templates/imagesFoldersViewerPanelContentItem.html',
                controller: 'imagesFoldersViewerPanelContentItemController',
                link: function($scope, $elem, $attrs) {}
            };
        })
        .controller('environmentPickerController', function($scope, Restangular) {
            $scope.environment = 'prodUrl';
            $scope.updateEnvironment = function(event) {
                $scope.environment = event.target.id;
                var id = event.target.id;
                var parent = $('#' + id).parent();
                var children = parent.children();
                for (var i = 0; i < children.length; i++) {
                    $(children[i]).removeClass('btn-primary');
                }
                $('#' + id).addClass('btn-primary');
            }
        })
        .directive('environmentPicker', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/environmentPicker.html',
                controller: 'environmentPickerController',
                link: function($scope, $elem, $attrs) {}
            };
        }])
        .controller('breadcrumbController', function($scope, Restangular) {

        })
        .directive('breadcrumb', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/breadcrumb.html',
                controller: 'breadcrumbController',
                link: function($scope, $elem, $attrs) {}
            };
        }])
        .controller('modalController', function($scope, Restangular) {

        })
        .directive('modal', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/modal.html',
                controller: 'modalController',
                link: function($scope, $elem, $attrs) {}
            };
        }])
        .controller('defaultFolderController', function($scope, Restangular) {
            console.log($scope.activeParent);
        })
        .directive('defaultFolder', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/defaultFolder.html',
                controller: 'defaultFolderController',
                link: function($scope, $elem, $attrs) {}
            };
        }])
})();
