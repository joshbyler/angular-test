(function() {

    angular.module('imagePicker', ['restangular', 'angular-loading-bar'])
        .config(['RestangularProvider', function(RestangularProvider) {

            RestangularProvider.setBaseUrl('http://cmsapi.bmwusa.williams-forrest.com/api/cms/');
        }])
        .controller('modalController', function($scope, Restangular) {
            if (!$scope.imagePickerLibrary) {
                $scope.imagePickerLibrary = {};
                $scope.imagePickerLibrary.parentFolders = [];

                //grab all the folders
                var allFolders = Restangular.one('folder');
                var assetFolderID;
                allFolders.get({foldersOnly: true}).then(function(data) {

                    //grab the assets folder id
                    for (var i = 0; i < data['childFolders'].length; i++) {
                        if (data['childFolders'][i].name === 'assets') {
                            assetFolderID = data['childFolders'][i].id;
                            $scope.assetFolderID = assetFolderID;
                            getAssetFolder();
                            break;
                        }
                    }
                });

                //grab the asset folder
                function getAssetFolder() {
                    var imagesFolderID;
                    var assetFolder = Restangular.one('folder/' + $scope.assetFolderID);
                    //grab the images folder
                    assetFolder.get().then(function(data) {
                        for (var i = 0; i < data['childFolders'].length; i++) {
                            if (data['childFolders'][i].name === 'images') {
                                imagesFolderID = data['childFolders'][i].id;
                                $scope.imagesFolderID = imagesFolderID;
                                getImagesFolder();
                                break;
                            }
                        }
                    });
                }

                //so now we grab all the folders inside the images folder
                function getImagesFolder() {
                    var imagesFolder = Restangular.one('folder/' + $scope.imagesFolderID);
                    imagesFolder.get().then(function(data) {
                        $scope.activeParent = data['childFolders'][0].id;
                        $scope.imagePickerLibrary.childFolders = data['childFolders'];
                        $scope.imagePickerLibrary.allImageFolders = data['childFolders'];
                        getActiveParent();
                    });
                }

                function getActiveParent() {
                    var activeParentFolder = Restangular.one('folder/' + $scope.activeParent);

                    activeParentFolder.get().then(function(data) {
                        $scope.activeParentName = data.name;
                        $scope.imagePickerLibrary.childFolders = data['childFolders'];
                        checkForKids($scope.activeParent);
                    });
                }

                function checkForKids(parent) {
                    Restangular.one('folder/' + parent + '?foldersOnly=true').get().then(function(data) {
                        if (data.childFolders.length > 0) {
                            $scope.imagePickerLibrary.childFolders = data['childFolders'];
                            $scope.activeParent = data.id;
                            $scope.activeParentName = data.name;
                            $scope.activeParentParent = data.parentFolder.id;
                            $scope.activeParentParentName = data.parentFolder.name;
                        }
                    });
                }

                $scope.updateCurrentParent = function(id, event) {
                    event.preventDefault();
                    if (id && id !== $scope.activeParent) {
                        $scope.activeParent = id;

                        //toggle all the folders so that only the currently selected folder is open
                        $('.list-group-item a').removeClass('collapsed').addClass('collapsed');
                        event.currentTarget.classList.toggle('collapsed');
                        checkForKids($scope.activeParent);
                    }
                }

                $scope.clearParentFolders = function(id, event) {
                    event.preventDefault();
                    $scope.activeParent = id;
                    $scope.imagePickerLibrary.parentFolders = [];
                    getActiveParent();
                    $('.left-header').children().show();
                    $('.parent-folders').removeClass('scroll');
                }

                $scope.sendUrlBack = function(event) {
                    event.preventDefault();

                    if ($scope.url !== '' || $scope.url !== undefined) {
                        $('#pick-me').parent().children('input')[0].value = $scope.url;
                        $('#imagePickerModal').modal('hide');
                        $('#pick-me').attr('id', '');
                    }
                }



                $scope.$watch('activeParent', function(newValue, oldValue) {
                    //have to check of oldValue is undefined because when the application first loads it is undefined
                    if(newValue !== oldValue && oldValue !== undefined && $scope.imagePickerLibrary.parentFolders.length === 0) {
                        Restangular.one('folder/', newValue).get().then(function(data){
                            //If we don't have any child folders, then we're at the images level, and need to put that up in the $scope
                            if (data['childFolders'].length === 0) {
                                $scope.imagePickerLibrary.innerImages = data['documents'];
                                $('.images').addClass('scroll');
                            }
                        });
                    }
                    // checkForChildren(newValue);
                });

                //special case for the z4 folder.  it's sturcture is different
                $scope.$watch('activeParentName', function() {
                    if ($scope.activeParentName === 'Z4') {
                        //get the current parent id, and then call the page again
                        //and dispurse the data correctly.
                        Restangular.one('folder/', $scope.activeParent).get().then(function(data) {
                            $scope.imagePickerLibrary.childFolders = data['childFolders'];
                            $scope.imagePickerLibrary.innerImages = data.childFolders[0].documents;
                        });
                    }
                });
            }
        })
        .directive('modal', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/modal.html',
                controller: 'modalController',
                link: function($scope, $elem, $attrs) {}
            }
        }])
        .controller('currentFolderController', function($scope, Restangular) {

        })
        .directive('currentFolder', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/currentFolder.html',
                controller: 'currentFolderController',
                link: function($scope, $elem, $attrs) {}
            }
        }])
        .controller('childFoldersController', function($scope, Restangular) {

        })
        .directive('childFolders', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/childFolders.html',
                controller: 'childFoldersController',
                link: function($scope, $elem, $attrs) {}
            }
        }])
        .controller('environmentPickerController', function($scope, Restangular) {
            $scope.environment = 'prodUrl';
            $scope.updateEnvironment = function(event) {
                $scope.environment = event.target.id;
                var id = event.target.id;
                var parent = $('#' + id).parent();
                var children = parent.children();
                for (var i = 0; i < children.length; i++) {
                    $(children[i]).removeClass('btn-primary');
                }
                $('#' + id).addClass('btn-primary');
            }
        })
        .directive('environmentPicker', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/environmentPicker.html',
                controller: 'environmentPickerController',
                link: function($scope, $elem, $attrs) {}
            }
        }])
        .controller('imagesController', function($scope, Restangular) {
            $scope.updateURL = function(image, event) {
                event.preventDefault();
                var environment = $scope.environment;
                var prodUrl = image.prodUrl;
                var designUrl = image.designUrl;
                var uatUrl = image.uatUrl;

                if ($scope.environment === 'uatUrl') {
                    $scope.url = uatUrl;
                } else if ($scope.environment === 'prodUrl') {
                    $scope.url = prodUrl;
                } else if ($scope.environment === 'designUrl') {
                    $scope.url = designUrl;
                }

                //remove selected class
                $('.image-holder').removeClass('selected-image');

                //add class to show that it has been selected
                $(event.currentTarget).addClass('selected-image');
            }
        })
        .directive('images', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/images.html',
                controller: 'imagesController',
                link: function($scope, $elem, $attrs) {}
            }
        }])
        .controller('parentFoldersController', function($scope, Restangular) {
            $scope.goUp = function(event) {
                event.preventDefault();
                var showParents;

                for (var i = 0; i < $scope.imagePickerLibrary.allImageFolders.length; i++) {
                    if ($scope.imagePickerLibrary.allImageFolders[i].name === $scope.activeParentName) {
                        showParents = true;
                        break;
                    }
                }

                if (showParents) {
                    $('.left-header').children().hide();
                    $('.parent-folders').addClass('scroll');
                    //pull back the parent folder, clear the child folders, and clear the inner images
                    Restangular.one('folder/' + $scope.imagesFolderID).get().then(function
                    (data) {
                        $scope.imagePickerLibrary.childFolders = [];
                        $scope.imagePickerLibrary.innerImages = [];

                        $scope.imagePickerLibrary.parentFolders = data['childFolders'];
                        $scope.activeParent = data['id'];
                        $scope.imagePickerLibrary.activeParentChildFolders = '';
                    });
                } else {
                    Restangular.one('folder/' + $scope.activeParentParent).get().then(function(data) {
                        $scope.imagePickerLibrary.childFolders = data.childFolders;
                        $scope.activeParentName = data.name;
                    });
                }

            }


        })
        .directive('parentFolders', ['$document', function($document) {
            return {
                replace: true,
                restrict: 'E',
                templateUrl: 'templates/parentFolders.html',
                controller: 'parentFoldersController',
                link: function($scope, $elem, $attrs) {}
            }
        }])

})();
