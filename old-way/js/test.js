angular.module('bmw.cms.ui.files.app', ['restangular'])
    .config(['RestangularProvider', function(RestangularProvider) {
	RestangularProvider.setBaseUrl('http://cmsapi.bmwusa.williams-forrest.com/api/cms/');
    }])
    .filter('editURL', function() {
        return function(path, name) {
            path = path || '';
            var editURL = '',
                re = /pages/;

            if (name === 'page.xml' && path.indexOf('pages/') > -1) {
                editURL = '/bmw/cms'+path.replace(re, '');
            } else {
                editURL = '#';
            }

            return editURL;
        };
    })
    .filter('editPageTitle', function() {
        return function(fileName, folderName) {
            var editURL = '';

            fileName = fileName || '';

            if (fileName === 'page.xml' && folderName) {
                fileName = 'Edit Page';
            }

            return fileName;
        };
    })
    .controller('FoldersFilesViewerController', function($scope, Restangular){
        if(!$scope.documentLibrary) {
            $scope.documentLibrary = {};

            /*$scope.documentLibrary.contents = DocumentLibrary.get(function(data){
                $scope.activeParent= data['id'];
            });*/

            var allFolders = Restangular.one('folder');
            allFolders.get({foldersOnly: true}).then(function(data) {
                $scope.activeParent= data['id'];
                $scope.documentLibrary.childFolders = data['childFolders'];
            });

            $scope.$watch('activeParent', function(newValue, oldValue){
                if(newValue !== oldValue) {
                    /*$scope.folder = Folder.get({id: newValue}, function(data){
                        $scope.folderView = data;
                    });*/
                    Restangular.one('folder', newValue).get().then(function(data){
                        $scope.folderView = data.plain();
                        $scope.folderContext = data['contextPath'];
                    });
                }
            });
        }
        $scope.updateCurrentParent = function(id, event) {
            event.preventDefault();

            if (id && id !== $scope.activeParent) {
                $scope.activeParent = id;
                $scope.$broadcast('parent-updated', {activeParent: id});
                event.currentTarget.classList.toggle('collapsed');
            }
        }
    })
    .directive('foldersViewer', ['$document', function($document) {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: '/bmw/javascripts/cms/templates/directives/foldersViewer.html',
            controller: 'FoldersFilesViewerController',
            link: function($scope, $elem, $attrs) {

            }
        };
    }])
    .directive('foldersViewerPanelHeading', function() {
        return {
            replace: false,
            restrict: 'A',
            templateUrl: '/bmw/javascripts/cms/templates/directives/foldersViewerPanelHeading.html',
            controller: function () {

            },
            link: function($scope, $elem, $attrs) {
            }
        };
    })
    .directive('foldersViewerPanelContent', function() {
        return {
            replace: false,
            restrict: 'A',
            templateUrl: '/bmw/javascripts/cms/templates/directives/foldersViewerPanelContent.html',
            controller: function () {

            },
            link: function($scope, $elem, $attrs) {
                $scope.$on('parent-updated', function(e, args){
                    if ($scope.folder.id === args.activeParent) {
                        console.log("yea");
                    }
                });
            }
        };
    })
    .directive('foldersViewerPanelContentItem', function() {
        return {
            replace: false,
            restrict: 'A',
            templateUrl: '/bmw/javascripts/cms/templates/directives/foldersViewerPanelContentItem.html',
            controller: function () {

            },
            link: function($scope, $elem, $attrs) {

            }
        };
    })
    .directive('filesViewer', function() {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: '/bmw/javascripts/cms/templates/directives/filesViewer.html',
            controller: 'FoldersFilesViewerController',
            link: function($scope, $elem, $attrs) {

            }
        };
    })
    .directive('filesViewerMenu', function() {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: '/bmw/javascripts/cms/templates/directives/filesViewerMenu.html',
            link: function($scope, $elem, $attrs) {

            }
        };
    })
    .directive('filesViewerCrumbs', function() {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: '/bmw/javascripts/cms/templates/directives/filesViewerCrumbs.html',
            link: function($scope, $elem, $attrs) {

            }
        };
    });
