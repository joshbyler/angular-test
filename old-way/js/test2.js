angular.module('bmw.cms.ui.library', ['ngResource'])
.factory('Library', function ($resource) {
    return $resource('http://dev.bmwusa-api.williams-forrest.com:81/api/cms/folder', {foldersOnly: true }, {});
})
.controller('FoldersViewerController', ['Library', '$scope', function(Library, $scope){
    $scope.rootFolder = Library.get();
}])
.directive('foldersViewer', ['$document', function($document) {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: '/bmw/javascripts/cms/templates/directives/foldersViewer.html',
        controller: 'FoldersViewerController',
        link: function($scope, $elem, $attrs) {}
    };
}]);
